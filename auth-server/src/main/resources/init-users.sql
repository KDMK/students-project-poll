INSERT INTO USERS(email, first_name, last_name, password, username) values ('admin@test', 'admin',
  'adminovic', '$2a$04$l6/HL2q2da5LRBD260dbV.cK0vHT.qQ5MYbAXjHS/4HQOFKNqiXwm', 'admin');

INSERT INTO ROLES(ROLE) VALUES ('ROLE_ADMIN');
INSERT INTO ROLES(ROLE) VALUES ('ROLE_MODERATOR');
INSERT INTO ROLES(ROLE) VALUES ('ROLE_USER');

INSERT INTO USER_ROLES(ROLE_ID,USER_ID) VALUES (1,1);

INSERT INTO OAUTH_CLIENT_DETAILS
    (client_id,
     client_secret,
     authorized_grant_types,
     access_token_validity,
     web_server_redirect_uri,
     scope) VALUES (
                    'project-pools',
                    '$2a$10$9H8nkCSsgeAL/eJCqn7dD.ckevQul0N8nHZMqTZqRw1FGbm85L186',
                    'authorization_code,password',
                    3600,
                    'http://localhost:8080/login,http://localhost:8080/app/oauth2/callback/local',
                    'read,write') -- sample
