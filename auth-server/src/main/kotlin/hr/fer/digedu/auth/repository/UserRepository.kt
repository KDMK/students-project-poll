package hr.fer.digedu.auth.repository

import hr.fer.digedu.auth.domain.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface UserRepository: JpaRepository<User, Long> {

    fun findByUsername(username: String): User?

    fun findById(id: Long?): Optional<User?>?

    fun getByResetToken(resetToken: String): User?
}
