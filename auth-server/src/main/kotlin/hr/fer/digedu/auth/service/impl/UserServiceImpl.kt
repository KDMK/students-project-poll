package hr.fer.digedu.auth.service.impl

import hr.fer.digedu.auth.domain.Role
import hr.fer.digedu.auth.domain.User
import hr.fer.digedu.auth.repository.RoleRepository
import hr.fer.digedu.auth.repository.UserRepository
import hr.fer.digedu.auth.service.EmailService
import hr.fer.digedu.auth.service.UserService
import hr.fer.digedu.auth.web.dto.RegistrationDTO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.ApplicationContext
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import java.util.*

@Service("userService")
class UserServiceImpl : UserDetailsService, UserService {

    @Autowired
    private lateinit var roleRepository: RoleRepository

    @Autowired
    private lateinit var userRepository: UserRepository

    @Autowired
    private lateinit var emailService: EmailService

    @Autowired
    private lateinit var bCryptPasswordEncoder: BCryptPasswordEncoder

    @Autowired
    private lateinit var applicationContext: ApplicationContext

    @Value("\${app.url}")
    private lateinit var appUrl: String

    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(username: String): UserDetails {
        val user = userRepository.findByUsername(username) ?: throw UsernameNotFoundException(
                "Invalid username or password")

        return org.springframework.security.core.userdetails.User(user.username, user.password, getAuthority(user))
    }

    override fun findAll(): List<User>? {
        return userRepository.findAll()
    }

    override fun save(user: User): User {
        return userRepository.save(user)
    }

    override fun delete(id: Long) {
        userRepository.delete(id)
    }

    override fun getByResetToken(resetToken: String): User? {
        return userRepository.getByResetToken(resetToken);
    }

    override fun getById(id: Long): User {
        return userRepository.getOne(id)
    }

    override fun registerUser(registrationDTO: RegistrationDTO) {
        val assignedRoles: List<Role>;
        if (!registrationDTO.roles.isEmpty()) {
            assignedRoles = registrationDTO.roles.map { roleRepository.findByRole(it) }
        } else {
            assignedRoles = listOf(roleRepository.findByRole("ROLE_USER"))
        }

        val user = User(registrationDTO.username, registrationDTO.firstName,
                registrationDTO.lastName,
                "",
                registrationDTO.email,
                assignedRoles)

        var sendMail = false
        if (registrationDTO.password.trim().isEmpty()) {
            user.resetToken = UUID.randomUUID().toString();
            sendMail = true
        } else {
            user.password = bCryptPasswordEncoder.encode(registrationDTO.password)
        }

        applicationContext.getBean(UserService::class.java).save(user)

        if (sendMail) {
            val params = mapOf("username" to user.username, "resetLink" to "${appUrl}/auth/reset-password?token=${user.resetToken}")
            val templateName = "mail-user-created"

            emailService.sendMail(user.email, "[Polly] Account created", params, templateName)
        }
    }

    override fun setResetToken(token: UUID, username: String) {
        val user = userRepository.findByUsername(username)
        user?.resetToken = token.toString()

        val params = mapOf("username" to user?.username!!, "resetLink" to "${appUrl}/auth/reset-password?token=${user.resetToken}")
        val templateName = "mail-reset-password"

        userRepository.save(user)
        emailService.sendMail(user.email, "[Polly] Reset password", params, templateName)
    }

    override fun findUser(username: String): User? {
        return userRepository.findByUsername(username)
    }

    private fun getAuthority(user: User): List<SimpleGrantedAuthority> {
        return user.roles.map { it.getSimpleAuthority() }
    }

}
