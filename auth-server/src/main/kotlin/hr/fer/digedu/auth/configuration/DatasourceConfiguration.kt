package hr.fer.digedu.auth.configuration

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.jdbc.datasource.DriverManagerDataSource

@Configuration
@ConfigurationProperties(prefix = "app.datasource.auth")
class DatasourceConfiguration {

    @Value("\${app.datasource.auth.username}")
    lateinit var databaseUsername: String

    @Value("\${app.datasource.auth.password}")
    lateinit var databasePassword: String

    @Value("\${app.datasource.auth.url}")
    lateinit var databaseFile: String

    @Bean(name = ["dataSource"])
    fun dataSource(): DriverManagerDataSource {
        val driverManagerDataSource = DriverManagerDataSource()
        driverManagerDataSource.setDriverClassName("org.h2.Driver")
        driverManagerDataSource.url = this.databaseFile
        driverManagerDataSource.username = this.databaseUsername
        driverManagerDataSource.password = this.databasePassword
        return driverManagerDataSource
    }

}
