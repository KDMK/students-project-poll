package hr.fer.digedu.auth.web.dto

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class RegistrationDTO(
        val username: String,
        val password: String,
        val firstName: String,
        val email: String,
        val lastName: String,
        val roles: List<String> = emptyList()
) {
    constructor() : this("", "", "", "", "")
}
