package hr.fer.digedu.auth.web.controller

import hr.fer.digedu.auth.domain.User
import hr.fer.digedu.auth.repository.RoleRepository
import hr.fer.digedu.auth.service.UserService
import hr.fer.digedu.auth.web.dto.EditUserDTO
import hr.fer.digedu.auth.web.dto.PasswordDTO
import hr.fer.digedu.auth.web.dto.RegistrationDTO
import hr.fer.digedu.auth.web.dto.UserDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.security.access.annotation.Secured
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import java.util.*

@RestController
@RequestMapping("/users")
class UserController {

    @Autowired
    lateinit var roleRepository: RoleRepository

    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var bCryptPasswordEncoder: BCryptPasswordEncoder

    @GetMapping
    @Secured("ROLE_ADMIN")
    fun listUser(): List<UserDto>? {
        val allUsers = userService.findAll()
        return allUsers?.map {
            UserDto(it.firstName, it.lastName, it.username, it.email, it.roles)
        }
    }

    @GetMapping("/exists/{username}")
    fun userExists(@PathVariable username: String): Boolean {
        val user = userService.findUser(username)
        return user != null
    }

    @PostMapping("/edit")
    @Secured("ROLE_ADMIN")
    fun editUser(@RequestBody editUserDTO: EditUserDTO): User? {
        val user = userService.findUser(editUserDTO.username) ?: return null

        user.firstName = editUserDTO.firstName
        user.lastName = editUserDTO.lastName
        user.email = editUserDTO.email
        user.roles = editUserDTO.roles.map { roleRepository.findByRole(it) }
        user.username = editUserDTO.username

        return userService.save(user)
    }

    @PostMapping("/reset")
    fun resetPassword(@RequestParam("token") resetToken: String, @RequestBody
    passwordDTO: PasswordDTO, model: Model): String? {
        val user = userService.getByResetToken(resetToken) ?: return null;
        user.password = bCryptPasswordEncoder.encode(passwordDTO.password)
        user.resetToken = null

        userService.save(user)
        return "OK"
    }

    @GetMapping("/reset/{username}")
    fun generateResetPasswordToken(@PathVariable("username")
                                   username: String): ResponseEntity<String> {
        val token = UUID.randomUUID()
        userService.setResetToken(token, username);

        return ResponseEntity.ok(token.toString());
    }

    @DeleteMapping("/{id}")
    @Secured("ROLE_ADMIN")
    fun deleteUser(@PathVariable("id") id: Long): ResponseEntity<String> {
        val auth = SecurityContextHolder.getContext().authentication
        val authenticatedUsername = (auth.principal as org.springframework.security.core.userdetails.User).username

        val user = userService.getById(id);
        if (user.username == authenticatedUsername) {
            return ResponseEntity.badRequest()
                    .body("Cannot delete logged in user.")
        }

        userService.delete(id);
        return ResponseEntity.ok().build()
    }

    @PostMapping("/upload-csv")
    fun ingestDataFile(@RequestParam("file")
                       file: MultipartFile): ResponseEntity<Map<String, Int>> {
        val fileContent = String(file.bytes, Charsets.UTF_8)

        var entries = fileContent.split("\n")
        entries = entries.subList(1, entries.size)

        var success = 0
        var failed = 0
        entries.forEach ent@{
            if (it.trim().isEmpty()) {
                return@ent
            }
            val parts = it.split(",")
            if (parts.size != 5) {
                failed++
                return@ent
            }

            val registrationDTO = RegistrationDTO(
                    parts[2],
                    "",
                    parts[0],
                    parts[3],
                    parts[1],
                    parts[4].split(";")

            )

            try {
                userService.registerUser(registrationDTO)
                success++
            } catch (e: Exception) {
                failed++
            }
        }

        return ResponseEntity.ok(
                mapOf("success" to success, "failed" to failed))
    }
}
