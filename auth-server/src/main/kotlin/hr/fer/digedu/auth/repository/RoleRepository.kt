package hr.fer.digedu.auth.repository

import hr.fer.digedu.auth.domain.Role
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface RoleRepository: JpaRepository<Role, Long> {

    fun findByRole(role: String): Role
}
