package hr.fer.digedu.auth.web.controller

import hr.fer.digedu.auth.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping("/admin")
class AdminController {

    @Autowired
    lateinit var userService: UserService

    @GetMapping("/users")
    fun showAllUsers(model: Model): String {
        model.addAttribute("users", userService.findAll())
        return "admin/users"
    }

}
