package hr.fer.digedu.auth.web.dto

import hr.fer.digedu.auth.domain.Role

data class UserDto(
        var firstName: String = "",
        var lastName: String = "",
        var username: String = "",
        var email: String = "",
        var roles: List<Role> = emptyList()
)
