package hr.fer.digedu.auth.web.controller

import hr.fer.digedu.auth.domain.Role
import hr.fer.digedu.auth.domain.User
import hr.fer.digedu.auth.repository.RoleRepository
import hr.fer.digedu.auth.service.EmailService
import hr.fer.digedu.auth.service.UserService
import hr.fer.digedu.auth.web.dto.RegistrationDTO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.oauth2.provider.token.TokenStore
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*
import java.security.Principal
import java.util.*
import javax.servlet.http.HttpServletRequest

@Controller
class AuthenticationController {

    @Autowired
    lateinit var tokenStore: TokenStore

    @Autowired
    lateinit var bCryptPasswordEncoder: BCryptPasswordEncoder

    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var roleRepository: RoleRepository

    @Autowired
    lateinit var emailService: EmailService

    @Value("\${app.url}")
    lateinit var appUrl: String

    @RequestMapping("/register/successful", method = [RequestMethod.GET])
    fun successfulLogin(): String = "successful-registration"

    @RequestMapping("/reset/successful", method = [RequestMethod.GET])
    fun successfulPasswordReset(): String = "successful-password-reset"

    @RequestMapping("/reset/successful-request", method = [RequestMethod.GET])
    fun successfulPasswordResetRequest(): String = "successful-reset-request"

    @RequestMapping("/login", method = [RequestMethod.GET])
    fun login(principal: Principal?): String {
        return if (principal == null) "login" else "profile"
    }

    @RequestMapping("/profile", method = [RequestMethod.GET])
    fun profile(principal: Principal?): String {
        return if (principal != null) "profile" else "login"
    }

    @RequestMapping("/register", method = [RequestMethod.POST])
    fun register(@RequestBody
                 registrationDTO: RegistrationDTO): ResponseEntity<String> {
        userService.registerUser(registrationDTO)
        return ResponseEntity.ok("Succesfully created user")
    }

    @RequestMapping("/revoke-token", method = [RequestMethod.GET])
    @ResponseStatus(HttpStatus.OK)
    fun logout(request: HttpServletRequest) {
        val authHeader = request.getHeader("Authorization")
        if (authHeader != null) {
            val tokenValue = authHeader.replace("Bearer", "")
                    .trim { it <= ' ' }
            val accessToken = tokenStore.readAccessToken(tokenValue)
            tokenStore.removeAccessToken(accessToken)
        }
    }

    @GetMapping("/reset-password")
    fun resetPasswordForm(@RequestParam("token") token: String, model: Model): String {
        model.addAttribute("token", token)
        return "reset-password"
    }
}
