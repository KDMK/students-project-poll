package hr.fer.digedu.auth.configuration

import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer

@Configuration
@EnableResourceServer
class ResourceServerConfig : ResourceServerConfigurerAdapter() {

    override fun configure(resources: ResourceServerSecurityConfigurer?) {
        resources!!.resourceId("resource_id").stateless(false)
    }

    override fun configure(http: HttpSecurity?) {
        http!!.requestMatchers().antMatchers("/user/**", "/logout")
                .and()
                .authorizeRequests().antMatchers("/users/sign_in").permitAll()
                .antMatchers(HttpMethod.POST, "/users/").permitAll()
                .anyRequest().authenticated()
                .and().logout().logoutUrl("/logout")

    }
}
