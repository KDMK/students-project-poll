package hr.fer.digedu.auth.web.controller

import hr.fer.digedu.auth.domain.UserWrapper
import hr.fer.digedu.auth.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class PrincipalController {

    @Autowired
    lateinit var userRepository: UserRepository

    @GetMapping("/user")
    fun user(principal: Authentication): UserWrapper {
        val wrapper = UserWrapper()
        val user = userRepository.findByUsername(principal.name)

        wrapper.setFullName("${user?.firstName} ${user?.lastName}")
                .setEmail(user?.email)
                .setUsername(user?.username)
                .setAuthorities(principal.authorities)
                .setId(user?.id.toString())
                .setDetails(principal.details)
                .setName(principal.name)

        return wrapper
    }
}
