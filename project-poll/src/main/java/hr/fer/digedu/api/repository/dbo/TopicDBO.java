package hr.fer.digedu.api.repository.dbo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import hr.fer.digedu.api.model.Status;
import hr.fer.digedu.api.model.TopicOrganizationType;
import hr.fer.digedu.api.model.User;
import hr.fer.digedu.api.model.topic.MultiLanguageTopicDescription;
import hr.fer.digedu.api.model.topic.MultiLanguageTopicTitle;
import hr.fer.digedu.api.model.topic.Topic;
import static hr.fer.digedu.api.repository.dbo.PollDBO.mapPollToDBO;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.stream.Collectors;
import org.hibernate.annotations.CreationTimestamp;

public class TopicDBO {

    private Long id;

    private String title;

    private String description;

    private Status status;

    private Boolean allowUnregister;

    private User createdBy;

    private Integer minNumberOfStudents = 1;

    private Integer maxNumberOfStudents;

    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate applicationStart;

    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate applicationEnd;

    private TopicOrganizationType topicOrganizationType;

    private Set<User> assignedUsers;

    @CreationTimestamp
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdAt;

    @CreationTimestamp
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updatedAt;

    private PollDBO poll;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Boolean getAllowUnregister() {
        return allowUnregister;
    }

    public void setAllowUnregister(Boolean allowUnregister) {
        this.allowUnregister = allowUnregister;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getMinNumberOfStudents() {
        return minNumberOfStudents;
    }

    public void setMinNumberOfStudents(Integer minNumberOfStudents) {
        this.minNumberOfStudents = minNumberOfStudents;
    }

    public Integer getMaxNumberOfStudents() {
        return maxNumberOfStudents;
    }

    public void setMaxNumberOfStudents(Integer maxNumberOfStudents) {
        this.maxNumberOfStudents = maxNumberOfStudents;
    }

    public LocalDate getApplicationStart() {
        return applicationStart;
    }

    public void setApplicationStart(LocalDate applicationStart) {
        this.applicationStart = applicationStart;
    }

    public LocalDate getApplicationEnd() {
        return applicationEnd;
    }

    public void setApplicationEnd(LocalDate applicationEnd) {
        this.applicationEnd = applicationEnd;
    }

    public TopicOrganizationType getTopicOrganizationType() {
        return topicOrganizationType;
    }

    public void setTopicOrganizationType(TopicOrganizationType topicOrganizationType) {
        this.topicOrganizationType = topicOrganizationType;
    }

    public Set<User> getAssignedUsers() {
        return assignedUsers;
    }

    public void setAssignedUsers(Set<User> assignedUsers) {
        this.assignedUsers = assignedUsers;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public PollDBO getPoll() {
        return poll;
    }

    public void setPoll(PollDBO poll) {
        this.poll = poll;
    }

    public static TopicDBO mapTopicToDBO(String lang, Topic topic, ObjectMapper objectMapper) {
        Set<MultiLanguageTopicTitle> title = topic.getTitles()
                .stream()
                .filter(multiLanguagePollTitle -> multiLanguagePollTitle.getLanguage().getCode().equalsIgnoreCase(lang))
                .collect(Collectors.toSet());

        if (title.size() == 0) {
            return null;
        }

        if (title.size() > 1) {
            throw new IllegalArgumentException("Title Set empty or not unique");
        }

        Set<MultiLanguageTopicDescription> description = topic.getDescriptions()
                .stream()
                .filter(multiLanguagePollDescription -> multiLanguagePollDescription.getLanguage().getCode()
                        .equalsIgnoreCase(lang))
                .collect(Collectors.toSet());

        if (description.size() != 1) {
            throw new IllegalArgumentException("Description Set empty or not unique");
        }

        TopicDBO mappedObject = objectMapper.convertValue(topic, TopicDBO.class);
        mappedObject.setTitle(((MultiLanguageTopicTitle) title.toArray()[0]).getValue());
        mappedObject.setDescription(((MultiLanguageTopicDescription) description.toArray()[0]).getValue());
        mappedObject.setPoll(mapPollToDBO(lang, topic.getPoll(), objectMapper));
        return mappedObject;

    }

}
