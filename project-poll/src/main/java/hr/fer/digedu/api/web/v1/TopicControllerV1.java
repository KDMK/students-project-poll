package hr.fer.digedu.api.web.v1;

import hr.fer.digedu.api.model.CsvImported;
import hr.fer.digedu.api.model.topic.Topic;
import hr.fer.digedu.api.repository.dbo.TopicDBO;
import hr.fer.digedu.api.service.TopicService;
import hr.fer.digedu.api.web.v1.dto.CreateTopicDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/v1/topic")
@Api(value = "Topic", tags = { "Topic" }, produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
public class TopicControllerV1 {

    private final TopicService topicService;

    public TopicControllerV1(TopicService topicService) {
        this.topicService = topicService;
    }

    @GetMapping
    @ApiOperation(value = "Finds all topics", response = Topic.class, responseContainer = "List")
    public ResponseEntity<List<TopicDBO>> getAll() {
        return ResponseEntity.ok(topicService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get single topic", response = Topic.class)
    public ResponseEntity<TopicDBO> getTopicById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(topicService.getById(id));
    }

    @PostMapping
    @ApiOperation(value = "Create topic", response = Topic.class)
    public ResponseEntity<Topic> createTopic(@Valid @RequestBody CreateTopicDto createTopicDto) {
        return ResponseEntity.ok(topicService.createTopic(createTopicDto));
    }

    @PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ApiOperation(value = "Upload CSV file with topics", response = List.class)
    public ResponseEntity<CsvImported> createTopicsFromCsv(
            @RequestParam("csv") MultipartFile csvFile,
            @RequestParam("pollId") Long pollId,
            @RequestParam("userId") Long userId
    ) {
        return ResponseEntity.ok(topicService.createTopicsFromCsv(csvFile, pollId, userId));
    }

    @PutMapping("/{topicId}/assign/{userId}")
    @ApiOperation(value = "Assign topic to user", response = Topic.class)
    public ResponseEntity<Topic> assignTopic(@PathVariable("topicId") Long topicId,
            @PathVariable("userId") Long userId) {
        return ResponseEntity.ok(topicService.assignTopicToUser(topicId, userId));
    }

    @GetMapping("/assigned/{userId}")
    @ApiOperation(value = "Get all topics assigned to user", response = Topic.class)
    public ResponseEntity<List<TopicDBO>> getTopicAssignedToUser(@PathVariable("userId") Long userId) {
        return ResponseEntity.ok(topicService.getTopicsAssignedToUser(userId));
    }

}
