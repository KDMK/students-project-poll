package hr.fer.digedu.api.web.v1;

import hr.fer.digedu.api.model.poll.Poll;
import hr.fer.digedu.api.model.topic.Topic;
import hr.fer.digedu.api.repository.dbo.PollDBO;
import hr.fer.digedu.api.repository.dbo.TopicDBO;
import hr.fer.digedu.api.service.PollService;
import hr.fer.digedu.api.web.v1.dto.CreatePollDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RequestMapping("/api/v1/poll")
@RestController
@Api(value = "/api/v1/poll", tags = { "Poll" }, produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
public class PollControllerV1 {

    private PollService pollService;

    public PollControllerV1(PollService pollService) {
        this.pollService = pollService;
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Finds all polls", response = Poll.class, responseContainer = "List")
    public ResponseEntity<List<PollDBO>> getAllPolls() {
        return ResponseEntity.ok(pollService.getAll());
    }

    @GetMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Find poll with given id", response = Poll.class)
    public ResponseEntity<PollDBO> getOne(@PathVariable("id") Long id) {
        return ResponseEntity.ok(pollService.getById(id));
    }

    @PostMapping(produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Creates new poll", response = Poll.class)
    public ResponseEntity<Poll> createPoll(@Valid @RequestBody CreatePollDto createPollDto) {
        return ResponseEntity.ok(pollService.create(createPollDto));
    }

    @GetMapping("/user/{userId}")
    @ApiOperation(value = "Finds all polls that are created by user", response = Poll.class, responseContainer = "List")
    public ResponseEntity<List<PollDBO>> getAllPolls(@PathVariable("userId") Long userId) {
        return ResponseEntity.ok(pollService.getPollsCreatedByUser(userId));
    }

    @PutMapping("/{pollId}/assign")
    @ApiOperation(value = "Grant poll access to users", response = Poll.class)
    public ResponseEntity<Poll> grantPollAccess(@PathVariable("pollId") Long pollId,
                                            @RequestParam("users") MultipartFile usersFile) {
        return ResponseEntity.ok(pollService.grantAccessToUsers(pollId, usersFile));
    }

    @GetMapping("/assigned/{userId}")
    @ApiOperation(value = "Get all polls that user has access to", response = Poll.class)
    public ResponseEntity<List<PollDBO>> getPollAssignedToUser(@PathVariable("userId") Long userId) {
        return ResponseEntity.ok(pollService.getAccessiblePolls(userId));
    }

}
