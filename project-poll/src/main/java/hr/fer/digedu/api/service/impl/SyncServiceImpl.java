package hr.fer.digedu.api.service.impl;

import hr.fer.digedu.api.configuration.security.model.AuthProvider;
import hr.fer.digedu.api.model.Role;
import hr.fer.digedu.api.model.User;
import hr.fer.digedu.api.repository.RoleRepository;
import hr.fer.digedu.api.repository.UserRepository;
import hr.fer.digedu.api.service.SyncService;
import hr.fer.digedu.api.web.v2.dto.UserDto;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class SyncServiceImpl implements SyncService {

    private final RestTemplate   restTemplate;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    @Value("${app.external.api.user}")
    private String userApiUrl;

    public SyncServiceImpl(RestTemplate restTemplate,
            UserRepository userRepository, RoleRepository roleRepository) {
        this.restTemplate = restTemplate;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    @Scheduled(cron = "0 /5 0 * * *")
    public void syncUsers() {
        UserDto[] usersDto = restTemplate.getForObject(userApiUrl, UserDto[].class);

        assert usersDto != null;
        for (UserDto userDto : usersDto) {
            Optional<User> existingUserWrapper = userRepository.findByEmail(userDto.getEmail());
            User targetUser;
            Set<Role> roles =
                    userDto.getRoles().stream().map(role -> roleRepository.findByRole(role.getRole())).collect(
                            Collectors.toSet());
            if (existingUserWrapper.isPresent()) {
                targetUser = existingUserWrapper.get();
                targetUser.setUsername(userDto.getUsername());
                targetUser.setEmail(userDto.getEmail());
                targetUser.setRoles(roles);
                targetUser.setName(String.format("%s %s", userDto.getFirstName(), userDto.getLastName()));
            } else {
                targetUser = new User();
                targetUser.setEmail(userDto.getEmail());
                targetUser.setUsername(userDto.getUsername());
                targetUser.setName(String.format("%s %s", userDto.getFirstName(), userDto.getLastName()));
                targetUser.setProvider(AuthProvider.local);
                targetUser.setRoles(roles);
                targetUser.setProviderId("1");
            }
            userRepository.save(targetUser);
        }
    }

}
