package hr.fer.digedu.api.web.v2.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import hr.fer.digedu.api.model.Status;
import hr.fer.digedu.api.model.TopicOrganizationType;
import java.time.LocalDate;
import java.util.List;
import javax.validation.constraints.Future;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class CreateMultiLanguageTopicDto {

    @NotNull
    @NotEmpty
    private List<TopicMultilanguageParams> multilanguageAttributes;

    @Positive
    private Integer minNumberOfStudents;

    @Positive
    private Integer maxNumberOfStudents;

    private Status status;

    private Boolean allowUnregister;

    private Long userId;

    @FutureOrPresent
    @NotNull
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate applicationStart;

    @Future
    @NotNull
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate applicationEnd;

    @NotNull
    private Long pollId;

    @NotNull
    private TopicOrganizationType topicOrganizationType;

    public List<TopicMultilanguageParams> getMultilanguageAttributes() {
        return multilanguageAttributes;
    }

    public void setMultilanguageAttributes(List<TopicMultilanguageParams> multilanguageAttributes) {
        this.multilanguageAttributes = multilanguageAttributes;
    }

    public Integer getMinNumberOfStudents() {
        return minNumberOfStudents;
    }

    public void setMinNumberOfStudents(Integer minNumberOfStudents) {
        this.minNumberOfStudents = minNumberOfStudents;
    }

    public Integer getMaxNumberOfStudents() {
        return maxNumberOfStudents;
    }

    public void setMaxNumberOfStudents(Integer maxNumberOfStudents) {
        this.maxNumberOfStudents = maxNumberOfStudents;
    }

    public LocalDate getApplicationStart() {
        return applicationStart;
    }

    public void setApplicationStart(LocalDate applicationStart) {
        this.applicationStart = applicationStart;
    }

    public LocalDate getApplicationEnd() {
        return applicationEnd;
    }

    public void setApplicationEnd(LocalDate applicationEnd) {
        this.applicationEnd = applicationEnd;
    }

    public Long getPollId() {
        return pollId;
    }

    public void setPollId(Long pollId) {
        this.pollId = pollId;
    }

    public TopicOrganizationType getTopicOrganizationType() {
        return topicOrganizationType;
    }

    public void setTopicOrganizationType(TopicOrganizationType topicOrganizationType) {
        this.topicOrganizationType = topicOrganizationType;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Boolean getAllowUnregister() {
        return allowUnregister;
    }

    public void setAllowUnregister(Boolean allowUnregister) {
        this.allowUnregister = allowUnregister;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public static class TopicMultilanguageParams implements MultilanguageParams {

        @NotNull
        private MultilanguageParamDto name;
        @NotNull
        private MultilanguageParamDto description;

        public TopicMultilanguageParams() {
        }

        public TopicMultilanguageParams(MultilanguageParamDto title, MultilanguageParamDto description) {
            this.name = title;
            this.description = description;
        }

        public MultilanguageParamDto getName() {
            return name;
        }

        public void setName(MultilanguageParamDto name) {
            this.name = name;
        }

        public MultilanguageParamDto getDescription() {
            return description;
        }

        public void setDescription(MultilanguageParamDto description) {
            this.description = description;
        }

    }

}
