package hr.fer.digedu.api.service;

import hr.fer.digedu.api.model.CsvImported;
import hr.fer.digedu.api.model.topic.Topic;
import hr.fer.digedu.api.repository.dbo.TopicDBO;
import hr.fer.digedu.api.web.v1.dto.CreateTopicDto;
import hr.fer.digedu.api.web.v2.dto.CreateMultiLanguageTopicDto;
import java.util.List;
import org.springframework.web.multipart.MultipartFile;

/**
 * Topic service implements all relevant methods for creation and manipulation of {@link Topic} entities`.
 */
public interface TopicService {

    /**
     * Finds all topics in database
     *
     * @return List of {@link Topic} object or empty if there is no entries in database
     */
    List<TopicDBO> getAll();

    /**
     * Finds topic by id and returns it.
     *
     * @param id of a topic
     * @return {@link Topic} object if found
     * @throws javax.persistence.EntityNotFoundException if there is no Topic with requested id
     */
    TopicDBO getById(Long id);

    /**
     * Finds all topics by language
     *
     * @param language desired language
     * @return List of Topics
     */
    List<TopicDBO> getAllByLanguage(String language);

    /**
     * Finds topic by id and language
     *
     * @param id of desired topic
     * @param language looking for
     * @return Topic if found
     */
    TopicDBO getByIdAndLanguage(Long id, String language);

    /**
     * Creates new topic.
     *
     * @param createTopicDto {@link CreateTopicDto}
     * @return {@link Topic} object if successfully persisted in database
     * @throws javax.persistence.EntityNotFoundException if there is no poll with given id
     */
    Topic createTopic(CreateTopicDto createTopicDto);

    /**
     * Creates new topic from multi-language DTOs.
     *
     * @param createMultiLanguageTopicDto {@link CreateTopicDto}
     * @return {@link Topic} object if successfully persisted in database
     * @throws javax.persistence.EntityNotFoundException if there is no poll with given id
     */
    Topic createTopic(CreateMultiLanguageTopicDto createMultiLanguageTopicDto);

    /**
     * Creates a new topics from CSV file. Dates are in format yyyy-MM-dd. Created topics are automatically in
     * status {@link hr.fer.digedu.api.model.Status} APPROVED.
     *
     * @param csvFile CSV file
     * @param pollId Poll to which belongs
     * @param userId User who added topics
     * @return {@link CsvImported} object with list of successfully uploaded {@link Topic}
     */
    CsvImported<Topic> createTopicsFromCsv(MultipartFile csvFile, Long pollId, Long userId);

    /**
     * Assigns given topic to selected user. If topic has all slots taken it shouldn't assign user and appropriate
     * message will be sent to user.
     *
     * @param topicId id of a topic that we want to update
     * @param userId id of a user that we are assigning
     * @return updated {@link Topic} object
     * @throws javax.persistence.EntityNotFoundException if either topic or user does not exist in database
     */
    Topic assignTopicToUser(Long topicId, Long userId);

    /**
     * Returns topics that are assigned to a user with the give id.
     *
     * @param userId id of a user
     * @return list of Topics
     */
    List<TopicDBO> getTopicsAssignedToUser(Long userId);

    /**
     * Returns topics that are assigned to a user with the give id.
     *
     * @param userId id of a user
     * @param language looking for
     * @return list of Topics
     */
    List<TopicDBO> getTopicsAssignedToUserByLanguage(Long userId, String language);

    /**
     * Returns topics that are part of given poll
     *
     * @param pollId id of a poll for which we want topics
     * @return List of topics
     */
    List<TopicDBO> getTopicsForPoll(Long pollId);

    /**
     * Returns topics that are part of given poll
     *
     * @param pollId id of a poll for which we want topics
     * @param language user language
     * @return List of topics
     */
    List<TopicDBO> getTopicsForPollByLanguage(Long pollId, String language);

    /**
     * Approve topic
     *
     * @param topicId id of a topic we want to approve
     * @return updated Topic DBO
     */
    TopicDBO approveTopic(Long topicId);

    /**
     * Deny topic
     *
     * @param topicId id of a topic we want to deny
     * @return updated Topic DBO
     */
    TopicDBO denyTopic(Long topicId);

    /**
     * Unassigns topic from user
     *
     * @param topicId id of target topic
     * @param userId id of user we want to remove
     * @return updated TopicDBO
     */
    TopicDBO unassignTopicFromUser(Long topicId, Long userId);

}
