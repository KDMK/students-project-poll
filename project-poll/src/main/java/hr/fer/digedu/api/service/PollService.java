package hr.fer.digedu.api.service;

import hr.fer.digedu.api.model.poll.Poll;
import hr.fer.digedu.api.repository.dbo.PollDBO;
import hr.fer.digedu.api.web.v1.dto.CreatePollDto;
import hr.fer.digedu.api.web.v2.dto.CreateMultiLanguagePollDto;
import java.util.List;
import org.springframework.web.multipart.MultipartFile;

/**
 * Poll service implements all relevant methods for creation and manipulation of {@link Poll} entities.
 */
public interface PollService {

    /**
     * Finds all polls in database
     *
     * @return List of {@link Poll} object or empty if there is no entries in database
     */
    List<PollDBO> getAll();

    /**
     * Finds all polls in database by given language
     *
     * @param language required language
     * @return List of {@link Poll} object or empty if there is no entries in database
     */
    List<PollDBO> getAllByLanguage(String language);

    /**
     * Finds poll by id and returns it.
     *
     * @param id of a poll
     * @return {@link Poll} object if found
     * @throws javax.persistence.EntityNotFoundException if there is no Poll with requested id
     */
    PollDBO getById(Long id);

    /**
     * Gets poll by id and language
     *
     * @param id of a poll
     * @param language desired language
     * @return Poll object if found
     */
    PollDBO getByIdAndLanguage(Long id, String language);

    /**
     * Creates new poll.
     *
     * @param createPollDto {@link CreatePollDto}
     * @return {@link Poll} object if successfully persisted in database
     * @throws javax.persistence.EntityNotFoundException if there is no poll with given id
     */
    Poll create(CreatePollDto createPollDto);

    /**
     * Creates new poll from multi-language DTOs.
     *
     * @param createMultiLanguagePollDto {@link CreatePollDto}
     * @return {@link Poll} object if successfully persisted in database
     * @throws javax.persistence.EntityNotFoundException if there is no poll with given id
     */
    Poll create(CreateMultiLanguagePollDto createMultiLanguagePollDto);

    /**
     * Updates poll.
     *
     * @param updatedPoll {@link Poll} object with updated values
     * @return {@link Poll} object if successfully persisted in database
     * @throws javax.persistence.EntityNotFoundException if there is no user with given id
     */
    Poll update(Poll updatedPoll);

    /**
     * Get all polls that are created b
     *
     * @return List of {@link Poll} object or empty if there is no entries in database
     */
    List<PollDBO> getPollsCreatedByUser(Long userId);

    /**
     * Finds all polls in database by given language
     *
     * @param language required language
     * @return List of {@link Poll} object or empty if there is no entries in database
     */
    List<PollDBO> getPollsCreatedByUserByLanguage(Long userId, String language);

    /**
     * Grants access to poll to users listed in file
     *
     * @param pollId id of a topic that we want to update
     * @param file with lines with username
     * @return updated {@link Poll} object
     * @throws javax.persistence.EntityNotFoundException if either poll or user does not exist in database
     */
    Poll grantAccessToUsers(Long pollId, MultipartFile file);

    /**
     * Returns polls that user with given id has access to
     *
     * @param userId id of a user
     * @return list of Polls
     */
    List<PollDBO> getAccessiblePolls(Long userId);

    /**
     * Returns polls that user with given id has access to
     *
     * @param userId id of a user
     * @param language looking for
     * @return list of Polls
     */
    List<PollDBO> getAccessiblePollsByLanguage(Long userId, String language);

    /**
     * Grants poll access to single user
     *
     * @param pollId id of a topic that we want to update
     * @param userId id of user we want to assign
     * @return updated {@link Poll} object
     * @throws javax.persistence.EntityNotFoundException if either poll or user does not exist in database
     */
    Poll grantAccessToUser(Long pollId, Long userId);

    /**
     * Revokes user access from selected poll
     *
     * @param pollId id of a poll we want to revoke access
     * @param userId id of a user we want to remove
     * @return update Poll DBO
     */
    PollDBO revokeAccessFromUser(Long pollId, Long userId);

}
