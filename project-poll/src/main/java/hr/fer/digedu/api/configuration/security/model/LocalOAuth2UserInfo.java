package hr.fer.digedu.api.configuration.security.model;

import java.util.Map;

public class LocalOAuth2UserInfo extends OAuth2UserInfo {

    public LocalOAuth2UserInfo(Map<String, Object> attributes) {
        super(attributes);
    }

    @Override
    public String getId() {
        return (String) attributes.get("id");
    }

    @Override
    public String getName() {
        return (String) attributes.get("fullName");
    }

    @Override
    public String getEmail() {
        return (String) attributes.get("email");
    }

    @Override
    public String getImageUrl() {
        return null;
    }

    @Override
    public String getUsername() {
        return (String) attributes.get("username");
    }

}
