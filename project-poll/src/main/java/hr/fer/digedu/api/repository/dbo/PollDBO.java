package hr.fer.digedu.api.repository.dbo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import hr.fer.digedu.api.model.TopicOrganizationType;
import static hr.fer.digedu.api.model.TopicOrganizationType.PARALLEL;
import hr.fer.digedu.api.model.User;
import hr.fer.digedu.api.model.poll.MultiLanguagePollDescription;
import hr.fer.digedu.api.model.poll.MultiLanguagePollTitle;
import hr.fer.digedu.api.model.poll.Poll;
import hr.fer.digedu.api.model.topic.Topic;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.stream.Collectors;

public class PollDBO {

    private Long id;

    private String title;

    private String description;

    private Set<Topic> topics;

    private User createdBy;

    private Boolean allowUnregister = false;

    private Boolean allowStudentsAddTopic = false;

    private TopicOrganizationType topicOrganizationType = PARALLEL;

    private Integer minNumberOfStudents;

    private Integer maxNumberOfStudents;

    private Set<User> assignedUsers;

    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate applicationStart;

    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate applicationEnd;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdAt;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updatedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Topic> getTopics() {
        return topics;
    }

    public void setTopics(Set<Topic> topics) {
        this.topics = topics;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Boolean getAllowUnregister() {
        return allowUnregister;
    }

    public void setAllowUnregister(Boolean allowUnregister) {
        this.allowUnregister = allowUnregister;
    }

    public Boolean getAllowStudentsAddTopic() {
        return allowStudentsAddTopic;
    }

    public void setAllowStudentsAddTopic(Boolean allowStudentsAddTopic) {
        this.allowStudentsAddTopic = allowStudentsAddTopic;
    }

    public TopicOrganizationType getTopicOrganizationType() {
        return topicOrganizationType;
    }

    public void setTopicOrganizationType(TopicOrganizationType topicOrganizationType) {
        this.topicOrganizationType = topicOrganizationType;
    }

    public Integer getMinNumberOfStudents() {
        return minNumberOfStudents;
    }

    public void setMinNumberOfStudents(Integer minNumberOfStudents) {
        this.minNumberOfStudents = minNumberOfStudents;
    }

    public Integer getMaxNumberOfStudents() {
        return maxNumberOfStudents;
    }

    public void setMaxNumberOfStudents(Integer maxNumberOfStudents) {
        this.maxNumberOfStudents = maxNumberOfStudents;
    }

    public LocalDate getApplicationStart() {
        return applicationStart;
    }

    public void setApplicationStart(LocalDate applicationStart) {
        this.applicationStart = applicationStart;
    }

    public LocalDate getApplicationEnd() {
        return applicationEnd;
    }

    public void setApplicationEnd(LocalDate applicationEnd) {
        this.applicationEnd = applicationEnd;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Set<User> getAssignedUsers() {
        return assignedUsers;
    }

    public void setAssignedUsers(Set<User> assignedUsers) {
        this.assignedUsers = assignedUsers;
    }

    public static PollDBO mapPollToDBO(String lang, Poll poll, ObjectMapper objectMapper) {
        Set<MultiLanguagePollTitle> title = poll.getTitles()
                .stream()
                .filter(multiLanguagePollTitle -> multiLanguagePollTitle.getLanguage().getCode().equalsIgnoreCase(lang))
                .collect(Collectors.toSet());

        if (title.size() == 0) {
            return null;
        }

        if (title.size() > 1) {
            throw new IllegalArgumentException("Title Set empty or not unique");
        }

        Set<MultiLanguagePollDescription> description = poll.getDescriptions()
                .stream()
                .filter(multiLanguagePollDescription -> multiLanguagePollDescription.getLanguage().getCode()
                        .equalsIgnoreCase(lang))
                .collect(Collectors.toSet());

        if (description.size() != 1) {
            throw new IllegalArgumentException("Description Set empty or not unique");
        }

        PollDBO mappedObject = objectMapper.convertValue(poll, PollDBO.class);
        mappedObject.setTitle(((MultiLanguagePollTitle) title.toArray()[0]).getValue());
        mappedObject.setDescription(((MultiLanguagePollDescription) description.toArray()[0]).getValue());
        mappedObject.setAssignedUsers(poll.getAccessPollUsers());
        mappedObject.setTopics(poll.getTopics());
        return mappedObject;
    }

}
