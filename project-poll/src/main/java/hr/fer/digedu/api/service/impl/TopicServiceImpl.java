package hr.fer.digedu.api.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.CSVReader;
import static hr.fer.digedu.api.model.Country.DEFAULT_LANGUAGE;
import hr.fer.digedu.api.model.CsvImported;
import hr.fer.digedu.api.model.MultiLanguageAttribute;
import hr.fer.digedu.api.model.Status;
import static hr.fer.digedu.api.model.Status.APPROVED;
import static hr.fer.digedu.api.model.Status.DENIED;
import hr.fer.digedu.api.model.TopicOrganizationType;
import hr.fer.digedu.api.model.User;
import hr.fer.digedu.api.model.poll.Poll;
import hr.fer.digedu.api.model.topic.MultiLanguageTopicDescription;
import hr.fer.digedu.api.model.topic.MultiLanguageTopicTitle;
import hr.fer.digedu.api.model.topic.Topic;
import hr.fer.digedu.api.repository.PollRepository;
import hr.fer.digedu.api.repository.TopicRepository;
import hr.fer.digedu.api.repository.dbo.TopicDBO;
import static hr.fer.digedu.api.repository.dbo.TopicDBO.mapTopicToDBO;
import hr.fer.digedu.api.service.CountryService;
import hr.fer.digedu.api.service.TopicService;
import hr.fer.digedu.api.service.UserService;
import hr.fer.digedu.api.web.v1.dto.CreateTopicDto;
import hr.fer.digedu.api.web.v2.dto.CreateMultiLanguageTopicDto;
import static hr.fer.digedu.api.web.v2.dto.CreateMultiLanguageTopicDto.TopicMultilanguageParams;
import hr.fer.digedu.api.web.v2.dto.MultilanguageParamDto;
import static hr.fer.digedu.api.web.v2.dto.MultilanguageParams.setLanguageForAttributes;
import java.io.IOException;
import java.io.StringReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.hibernate.Hibernate;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class TopicServiceImpl implements TopicService {

    private final TopicRepository    topicRepository;
    private final UserService        userService;
    private final PollRepository     pollRepository;
    private final CountryService     countryService;
    private final ApplicationContext applicationContext;

    private final ObjectMapper objectMapper;

    public TopicServiceImpl(TopicRepository topicRepository, UserService userService,
            PollRepository pollRepository, CountryService countryService,
            ApplicationContext applicationContext, ObjectMapper objectMapper) {
        this.topicRepository = topicRepository;
        this.userService = userService;
        this.pollRepository = pollRepository;
        this.countryService = countryService;
        this.applicationContext = applicationContext;
        this.objectMapper = objectMapper;
    }

    @Override
    public List<TopicDBO> getAll() {
        return applicationContext.getBean(TopicService.class).getAllByLanguage(DEFAULT_LANGUAGE);
    }

    @Override
    public TopicDBO getById(Long id) {
        return applicationContext.getBean(TopicService.class).getByIdAndLanguage(id, DEFAULT_LANGUAGE);
    }

    @Override
    public List<TopicDBO> getAllByLanguage(String language) {
        final String lang = language == null ? DEFAULT_LANGUAGE : language;
        List<Topic> topics = topicRepository.findAll();
        return topics.stream().map(topic -> mapTopicToDBO(lang, topic, objectMapper)).filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @Override
    public TopicDBO getByIdAndLanguage(Long id, String language) {
        final String lang = language == null ? DEFAULT_LANGUAGE : language;
        Topic topic = (Topic) Hibernate.unproxy(topicRepository.getOne(id));

        return mapTopicToDBO(lang, topic, objectMapper);
    }

    @Override
    public Topic createTopic(CreateTopicDto createTopicDto) {
        CreateMultiLanguageTopicDto topicDto = new CreateMultiLanguageTopicDto();

        topicDto.setApplicationStart(createTopicDto.getApplicationStart());
        topicDto.setApplicationEnd(createTopicDto.getApplicationEnd());

        topicDto.setMinNumberOfStudents(createTopicDto.getMinNumberOfStudents());
        topicDto.setMaxNumberOfStudents(createTopicDto.getMaxNumberOfStudents());

        topicDto.setPollId(createTopicDto.getPollId());
        topicDto.setTopicOrganizationType(createTopicDto.getType());
        topicDto.setAllowUnregister(createTopicDto.getAllowUnregister());
        topicDto.setStatus(createTopicDto.getStatus());
        topicDto.setUserId(createTopicDto.getUserId());

        MultilanguageParamDto title = new MultilanguageParamDto();
        MultilanguageParamDto description = new MultilanguageParamDto();

        title.setValue(createTopicDto.getTitle());
        title.setCode(DEFAULT_LANGUAGE);
        description.setValue(createTopicDto.getDescription());
        description.setCode(DEFAULT_LANGUAGE);

        TopicMultilanguageParams multilanguageParams = new TopicMultilanguageParams(title, description);
        topicDto.setMultilanguageAttributes(Collections.singletonList(multilanguageParams));

        return applicationContext.getBean(TopicService.class).createTopic(topicDto);
    }

    @Override
    public Topic createTopic(CreateMultiLanguageTopicDto createMultiLanguageTopicDto) {
        Topic topic = objectMapper.convertValue(createMultiLanguageTopicDto, Topic.class);
        Poll poll = pollRepository.getOne(createMultiLanguageTopicDto.getPollId());

        HashSet<MultiLanguageTopicTitle> titleList = new HashSet<>();
        HashSet<MultiLanguageTopicDescription> descriptionList = new HashSet<>();
        for (TopicMultilanguageParams attribute : createMultiLanguageTopicDto
                .getMultilanguageAttributes()) {
            MultiLanguageAttribute name = new MultiLanguageTopicTitle().setValue(attribute.getName().getValue());
            MultiLanguageAttribute description =
                    new MultiLanguageTopicDescription().setValue(attribute.getDescription().getValue());

            setLanguageForAttributes(attribute, name, description, countryService);
            titleList.add((MultiLanguageTopicTitle) name);
            descriptionList.add((MultiLanguageTopicDescription) description);
        }

        User user = userService.getById(createMultiLanguageTopicDto.getUserId());

        topic.setTitles(titleList);
        topic.setDescriptions(descriptionList);
        topic.setPoll(poll);
        topic.setTopicOrganizationType(createMultiLanguageTopicDto.getTopicOrganizationType());
        topic.setAllowUnregister(createMultiLanguageTopicDto.getAllowUnregister());
        topic.setCreatedBy(user);

        return topicRepository.save(topic);
    }

    @Override
    public CsvImported<Topic> createTopicsFromCsv(MultipartFile csvFile, Long pollId, Long userId) {
        CsvImported<Topic> topics = new CsvImported<>();

        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            CSVReader reader = new CSVReader(new StringReader(new String(csvFile.getBytes())));
            String[] line;
            while ((line = reader.readNext()) != null) {
                boolean isLineComplete = true;
                for (String s : line) {
                    if (s.isEmpty()) {
                        isLineComplete = false;
                        topics.increaseFailed();
                        break;
                    }
                }

                if (isLineComplete) {
                    CreateMultiLanguageTopicDto topicDto = new CreateMultiLanguageTopicDto();

                    topicDto.setApplicationStart(LocalDate.parse(line[2], formatter));
                    topicDto.setApplicationEnd(LocalDate.parse(line[3], formatter));

                    topicDto.setMinNumberOfStudents(Integer.parseInt(line[4]));
                    topicDto.setMaxNumberOfStudents(Integer.parseInt(line[5]));

                    topicDto.setPollId(pollId);
                    topicDto.setTopicOrganizationType(TopicOrganizationType.valueOf(line[6]));
                    topicDto.setAllowUnregister(Boolean.getBoolean(line[7]));
                    topicDto.setStatus(APPROVED);
                    topicDto.setUserId(userId);

                    MultilanguageParamDto title = new MultilanguageParamDto();
                    MultilanguageParamDto description = new MultilanguageParamDto();

                    title.setValue(line[0]);
                    title.setCode(DEFAULT_LANGUAGE);
                    description.setValue(line[1]);
                    description.setCode(DEFAULT_LANGUAGE);

                    TopicMultilanguageParams multilanguageParams = new TopicMultilanguageParams(title, description);
                    topicDto.setMultilanguageAttributes(Collections.singletonList(multilanguageParams));

                    topics.addSuccessful(applicationContext.getBean(TopicService.class).createTopic(topicDto));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return topics;
    }

    @Override
    public Topic assignTopicToUser(Long topicId, Long userId) {
        User user = userService.getById(userId);
        Topic topic = topicRepository.getOne(topicId);

        topic.getAssignedUsers().add(user);
        return topicRepository.save(topic);
    }

    @Override
    public List<TopicDBO> getTopicsAssignedToUser(Long userId) {
        return applicationContext.getBean(TopicService.class)
                .getTopicsAssignedToUserByLanguage(userId, DEFAULT_LANGUAGE);
    }

    @Override
    public List<TopicDBO> getTopicsAssignedToUserByLanguage(Long userId, String language) {
        final String lang = language == null ? DEFAULT_LANGUAGE : language;
        User user = userService.getById(userId);
        List<Topic> allTopics = topicRepository.findByAssignedUsersContaining(user);

        return allTopics.stream()
                .map(topic -> mapTopicToDBO(lang, topic, objectMapper)).filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @Override
    public List<TopicDBO> getTopicsForPoll(Long pollId) {
        return applicationContext.getBean(TopicService.class).getTopicsForPollByLanguage(pollId, DEFAULT_LANGUAGE);
    }

    @Override
    public List<TopicDBO> getTopicsForPollByLanguage(Long pollId, String language) {
        final String lang = language == null ? DEFAULT_LANGUAGE : language;
        List<Topic> topics = topicRepository.findByPollId(pollId);

        return topics.stream()
                .map(topic -> mapTopicToDBO(lang, topic, objectMapper))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @Override
    public TopicDBO approveTopic(Long topicId) {
        return updateTopicStatus(topicId, APPROVED);
    }

    @Override
    public TopicDBO denyTopic(Long topicId) {
        return updateTopicStatus(topicId, DENIED);
    }

    @Override
    public TopicDBO unassignTopicFromUser(Long topicId, Long userId) {
        User user = userService.getById(userId);
        Topic topic = topicRepository.getOne(topicId);

        if (topic.getAssignedUsers().stream().filter(user1 -> user1.equals(user)).count() == 1) {
            topic.getAssignedUsers().removeAll(Collections.singleton(user));
            topicRepository.save(topic);
        }

        return mapTopicToDBO(DEFAULT_LANGUAGE, topic, objectMapper);
    }

    private TopicDBO updateTopicStatus(Long topicId, Status topicStatus) {
        Topic topic = topicRepository.getOne(topicId);
        topic.setStatus(topicStatus);
        return mapTopicToDBO(DEFAULT_LANGUAGE, topicRepository.save(topic), objectMapper);
    }

}
