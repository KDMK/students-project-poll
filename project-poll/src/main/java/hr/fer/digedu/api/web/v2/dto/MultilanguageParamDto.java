package hr.fer.digedu.api.web.v2.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class MultilanguageParamDto {

    @NotNull
    @Size(min = 2, max = 2)
    private String code;

    @NotBlank
    @Size(max = 1024)
    private String value;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
