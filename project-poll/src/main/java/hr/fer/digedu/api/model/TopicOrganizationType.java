package hr.fer.digedu.api.model;

/**
 * The enum Topic organization type represent in which way students are grouped on topic.
 */
public enum TopicOrganizationType {
    /**
     * Group topic organization type marks topic that require group collaboration between all assigned students.
     */
    GROUP,
    /**
     * Parallel topic organization type marks topic in which every student writes its own assignment.
     */
    PARALLEL
}
