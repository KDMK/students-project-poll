package hr.fer.digedu.api.web.v2;

import hr.fer.digedu.api.model.CsvImported;
import hr.fer.digedu.api.model.topic.Topic;
import hr.fer.digedu.api.repository.dbo.TopicDBO;
import hr.fer.digedu.api.service.TopicService;
import hr.fer.digedu.api.web.v2.dto.CreateMultiLanguageTopicDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import org.springframework.http.MediaType;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/v2/topic")
@Api(value = "Topic", tags = { "Topic" }, produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
public class TopicControllerV2 {

    private final TopicService topicService;

    public TopicControllerV2(TopicService topicService) {
        this.topicService = topicService;
    }

    @GetMapping
    @ApiOperation(value = "Finds all topics", response = Topic.class, responseContainer = "List")
    public ResponseEntity<List<TopicDBO>> getAll(@RequestParam(required = false, value = "lang") String language) {
        return ResponseEntity.ok(topicService.getAllByLanguage(language));
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get single topic", response = Topic.class)
    public ResponseEntity<TopicDBO> getTopicById(@PathVariable("id") Long id,
            @RequestParam(required = false, value = "lang") String language) {
        return ResponseEntity.ok(topicService.getByIdAndLanguage(id, language));
    }

    @PostMapping
    @ApiOperation(value = "Create topic", response = Topic.class)
    public ResponseEntity<Topic> createTopic(@Valid @RequestBody
            CreateMultiLanguageTopicDto createMultiLanguageTopicDto) {
        return ResponseEntity.ok(topicService.createTopic(createMultiLanguageTopicDto));
    }

    @PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ApiOperation(value = "Upload CSV file with topics", response = List.class)
    public ResponseEntity<CsvImported<Topic>> createTopicsFromCsv(
            @RequestParam("csv") MultipartFile csvFile,
            @RequestParam("pollId") Long pollId,
            @RequestParam("userId") Long userId
    ) {
        return ResponseEntity.ok(topicService.createTopicsFromCsv(csvFile, pollId, userId));
    }

    @PutMapping("/{topicId}/assign/{userId}")
    @ApiOperation(value = "Assign topic to user", response = Topic.class)
    public ResponseEntity<Topic> assignTopic(@PathVariable("topicId") Long topicId,
            @PathVariable("userId") Long userId) {
        return ResponseEntity.ok(topicService.assignTopicToUser(topicId, userId));
    }

    @PutMapping("/{topicId}/remove/{userId}")
    @ApiOperation(value = "Unassign topic from user", response = TopicDBO.class)
    public ResponseEntity<TopicDBO> unassignTopic(@PathVariable("topicId") Long topicId,
            @PathVariable("userId") Long userId) {
        return ResponseEntity.ok(topicService.unassignTopicFromUser(topicId, userId));
    }

    @GetMapping("/assigned/{userId}")
    @ApiOperation(value = "Get all topics assigned to user", response = TopicDBO.class)
    public ResponseEntity<List<TopicDBO>> getTopicAssignedToUser(@PathVariable("userId") Long userId,
            @RequestParam(required = false, value = "lang") String language) {
        return ResponseEntity.ok(topicService.getTopicsAssignedToUserByLanguage(userId, language));
    }

    @PutMapping("/{topicId}/approve")
    @ApiOperation(value = "Approve created topic", response = TopicDBO.class)
    public ResponseEntity<TopicDBO> approveTopic(@PathVariable("topicId") Long topicId) {
        return ResponseEntity.ok(topicService.approveTopic(topicId));
    }

    @PutMapping("/{topicId}/deny")
    @ApiOperation(value = "Deny created topic", response = TopicDBO.class)
    public ResponseEntity<TopicDBO> denyTopic(@PathVariable("topicId") Long topicId) {
        return ResponseEntity.ok(topicService.denyTopic(topicId));
    }

}


