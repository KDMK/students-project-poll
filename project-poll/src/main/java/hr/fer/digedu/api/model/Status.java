package hr.fer.digedu.api.model;

public enum Status {

    APPROVED,

    WAITING_FOR_APPROVAL,

    DENIED
}
