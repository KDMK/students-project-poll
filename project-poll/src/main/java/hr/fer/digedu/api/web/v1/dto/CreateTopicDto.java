package hr.fer.digedu.api.web.v1.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import hr.fer.digedu.api.model.Status;
import hr.fer.digedu.api.model.TopicOrganizationType;
import java.time.LocalDate;
import javax.validation.constraints.Future;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateTopicDto {

    @NotNull
    private String title;

    @Size(max = 1024)
    private String description;

    private Status status;

    @NotNull
    private TopicOrganizationType type;

    private Boolean allowUnregister;

    private Long userId;

    @Positive
    private Integer minNumberOfStudents;

    @Positive
    private Integer maxNumberOfStudents;

    @FutureOrPresent
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate applicationStart;

    @Future
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate applicationEnd;

    @NotNull
    private Long pollId;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getApplicationStart() {
        return applicationStart;
    }

    public void setApplicationStart(LocalDate applicationStart) {
        this.applicationStart = applicationStart;
    }

    public LocalDate getApplicationEnd() {
        return applicationEnd;
    }

    public void setApplicationEnd(LocalDate applicationEnd) {
        this.applicationEnd = applicationEnd;
    }

    public Long getPollId() {
        return pollId;
    }

    public void setPollId(Long pollId) {
        this.pollId = pollId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public TopicOrganizationType getType() {
        return type;
    }

    public void setType(TopicOrganizationType type) {
        this.type = type;
    }

    public Boolean getAllowUnregister() {
        return allowUnregister;
    }

    public void setAllowUnregister(Boolean allowUnregister) {
        this.allowUnregister = allowUnregister;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getMinNumberOfStudents() {
        return minNumberOfStudents;
    }

    public void setMinNumberOfStudents(Integer minNumberOfStudents) {
        this.minNumberOfStudents = minNumberOfStudents;
    }

    public Integer getMaxNumberOfStudents() {
        return maxNumberOfStudents;
    }

    public void setMaxNumberOfStudents(Integer maxNumberOfStudents) {
        this.maxNumberOfStudents = maxNumberOfStudents;
    }

}
