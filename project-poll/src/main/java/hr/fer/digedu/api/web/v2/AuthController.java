package hr.fer.digedu.api.web.v2;

import hr.fer.digedu.api.configuration.security.CurrentUser;
import hr.fer.digedu.api.configuration.security.model.UserPrincipal;
import hr.fer.digedu.api.exception.ResourceNotFoundException;
import hr.fer.digedu.api.model.User;
import hr.fer.digedu.api.repository.UserRepository;
import hr.fer.digedu.api.web.v2.dto.PrincipalDto;
import hr.fer.digedu.api.web.v2.dto.UserDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.stream.Collectors;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v2/auth")
@Api(value = "/api/v2/auth", tags = { "Authentication" }, produces = APPLICATION_JSON_VALUE,
        consumes = APPLICATION_JSON_VALUE)
public class AuthController {

    private final UserRepository userRepository;

    public AuthController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/me")
    @ApiOperation(value = "Get information about currently logged user", response = UserDto.class)
    public PrincipalDto getCurrentUser(@CurrentUser UserPrincipal userPrincipal) {
        User user = userRepository.findById(userPrincipal.getId())
                .orElseThrow(() -> new ResourceNotFoundException("User", "id", userPrincipal.getId()));
        List<String> roles = userPrincipal
                .getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());


        return new PrincipalDto(user, roles);
    }

}
