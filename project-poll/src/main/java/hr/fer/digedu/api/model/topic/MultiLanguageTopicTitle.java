package hr.fer.digedu.api.model.topic;

import hr.fer.digedu.api.model.MultiLanguageAttribute;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class MultiLanguageTopicTitle extends MultiLanguageAttribute {
}
