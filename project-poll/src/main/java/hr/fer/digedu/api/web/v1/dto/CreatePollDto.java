package hr.fer.digedu.api.web.v1.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import java.time.LocalDate;
import javax.validation.constraints.Future;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class CreatePollDto {

    @NotNull
    @Size(max = 255)
    private String title;

    @Size(max = 1024)
    private String description;

    @NotNull
    private Long userId;

    @Positive
    private Integer minNumberOfStudents;

    @Positive
    private Integer maxNumberOfStudents;

    @FutureOrPresent
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate applicationStart;

    @Future
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate applicationEnd;

    private Boolean allowUnregister;

    @NotNull
    private Boolean allowStudentsAddTopic;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getMinNumberOfStudents() {
        return minNumberOfStudents;
    }

    public void setMinNumberOfStudents(Integer minNumberOfStudents) {
        this.minNumberOfStudents = minNumberOfStudents;
    }

    public Integer getMaxNumberOfStudents() {
        return maxNumberOfStudents;
    }

    public void setMaxNumberOfStudents(Integer maxNumberOfStudents) {
        this.maxNumberOfStudents = maxNumberOfStudents;
    }

    public LocalDate getApplicationStart() {
        return applicationStart;
    }

    public void setApplicationStart(LocalDate applicationStart) {
        this.applicationStart = applicationStart;
    }

    public LocalDate getApplicationEnd() {
        return applicationEnd;
    }

    public void setApplicationEnd(LocalDate applicationEnd) {
        this.applicationEnd = applicationEnd;
    }

    public Boolean getAllowUnregister() {
        return allowUnregister;
    }

    public void setAllowUnregister(Boolean allowUnregister) {
        this.allowUnregister = allowUnregister;
    }

    public Boolean getAllowStudentsAddTopic() {
        return allowStudentsAddTopic;
    }

    public void setAllowStudentsAddTopic(Boolean allowStudentsAddTopic) {
        this.allowStudentsAddTopic = allowStudentsAddTopic;
    }
}
