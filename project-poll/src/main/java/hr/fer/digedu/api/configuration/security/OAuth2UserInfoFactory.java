package hr.fer.digedu.api.configuration.security;

import hr.fer.digedu.api.configuration.security.model.AuthProvider;
import hr.fer.digedu.api.configuration.security.model.FacebookOAuth2UserInfo;
import hr.fer.digedu.api.configuration.security.model.LocalOAuth2UserInfo;
import hr.fer.digedu.api.configuration.security.model.OAuth2UserInfo;
import hr.fer.digedu.api.exception.OAuth2AuthenticationProcessingException;
import java.util.Map;

public class OAuth2UserInfoFactory {

    public static OAuth2UserInfo getOAuth2UserInfo(String registrationId, Map<String, Object> attributes)
            throws OAuth2AuthenticationProcessingException {
        if (registrationId.equalsIgnoreCase(AuthProvider.facebook.toString())) {
            return new FacebookOAuth2UserInfo(attributes);
        } else if (registrationId.equalsIgnoreCase(AuthProvider.local.toString())) {
            return new LocalOAuth2UserInfo(attributes);
        } else {
            throw new OAuth2AuthenticationProcessingException(
                    "Sorry! Login with " + registrationId + " is not supported yet.");
        }
    }

}
