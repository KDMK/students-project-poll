package hr.fer.digedu.api.configuration;

import com.google.common.base.Predicates;
import java.util.Collections;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        Tag userTag = new Tag("User", "Methods for fetching and updating data about the users");
        Tag pollTag = new Tag("Poll", "Methods for fetching and manipulating data about the polls");
        Tag topicTag = new Tag("Topic", "Methods for fetching and manipulating data about the topics");
        Tag countryTag = new Tag("Country", "Methods for fetching data about the countries");
        Tag authenticationTag = new Tag("Authentication", "Methods for handling authentication related tasks");
        Tag syncTag = new Tag("Sync", "Controller for performin various sync tasks");

        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(Predicates.not(PathSelectors.regex("/error.*")))
                .build()
                .tags(userTag, pollTag, topicTag, countryTag, authenticationTag)
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        Contact contact = new Contact("Tim Raketa", "https://gitlab.com/SandraKuzmic/students-project-poll",
                "tim-raketa@just.kidding");
        return new ApiInfo(
                "Student polls API",
                "API for students polls project for Digital education course at FER",
                "v1",
                "Terms of service",
                contact,
                "MIT", "https://opensource.org/licenses/MIT", Collections.emptyList());
    }

}
