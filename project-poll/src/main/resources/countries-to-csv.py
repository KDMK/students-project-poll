import json

with open('countries.json') as json_file:
    data = json.load(json_file)
    csv = "code,name\n"
    for p in data:
      csv += "{},{}\n".format(p['code'].upper(), p['name'])

with open('countries.csv', 'w') as outfile:
    outfile.write(csv)
