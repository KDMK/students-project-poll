import React, { useEffect, useState } from "react";
import { useParams } from "react-router";

import { isPast, isFuture } from "date-fns";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import { makeStyles } from "@material-ui/core/styles";
import { fetchPollById } from "services/poll";
import Topic from "components/Topic";
import AddTopicModalButton from "components/Modals/AddTopic/AddTopic";
import { COLORS } from "constants/colors";
import AddCSVModalButton from "components/Modals/AddCSV/AddCSV";
import { fetchTopicsByPollId, getTopicsAssignedToUser } from "../services/topic";
import AssignUserModalButton from "../components/Modals/AssignUser/AssignUser";
import { request } from "../util/APIUtils";
import { dateFormatting} from "../services/helpers";

const useStyles = makeStyles({
  root: {},
  title: {
    fontSize: 40,
    padding: 30,
    fontWeight: 400,
    paddingLeft: 0
  },
  row: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    paddingTop: 32,
    paddingBottom: 32
  },
  buttons: {
    display: "flex"
  },
  pollText: {
    fontSize: 14,
    marginBottom: 8,
    position: "relative",
    color: COLORS.gray_5
  },
  button: {
    height: 40
  },
  rightPart: {
    display: "flex"
  }
});

export const fetchAllUsers = id =>
  request({ url: `/app/api/v1/user/${id}/assigned` });

export default function PollScreen({ user }) {
  const classes = useStyles();
  const params = useParams();
  const [poll, setPoll] = useState({});
  const [topics, setTopics] = useState([]);
  const [userTopics, setUserTopics] = useState([]);
  const [allUsers, setAllUsers] = useState([]);

  function isAdmin() {
    if (typeof user.roles !== "undefined") {
      return user.roles.indexOf("ROLE_ADMIN") !== -1;
    }
    return false;
  }

  function isModerator() {
    if (typeof user.roles !== "undefined") {
      return user.roles.indexOf("ROLE_MODERATOR") !== -1;
    }
    return false;
  }
  useEffect(() => {
    fetchPollById(params.projektId)
      .then(poll => {
        setPoll(poll);
        fetchAllUsers(user.id).then(users => {
          let userIds = poll.assignedUsers.map(user => user.id);
          setAllUsers(users.filter(user => userIds.indexOf(user.id) === -1));
        });
      })
      .catch(console.error);
    fetchTopicsByPollId(params.projektId).then(topics => {
      setTopics(topics);
    });
    getTopicsAssignedToUser(user.id).then(userTopics => setUserTopics(userTopics));
  }, [params.projektId, user.id]);

  const renderTopics = () =>
    topics.map(topic =>
      <Topic
        key={topic.id}
        topicId={topic.id}
        userId={user.id}
        title={topic.title}
        description={topic.description}
        status={topic.status}
        topicOrganizationType={topic.topicOrganizationType}
        assignedUsers={topic.assignedUsers}
        maxStudentCount={topic.maxNumberOfStudents}
        deadline={topic.applicationEnd}
        suggestedBy={topic.createdBy.name}
        startDay={topic.applicationStart}
        user={user}
        userTopics={userTopics}
      />
    );

  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="md" className={classes.root}>
        <Typography variant="h2" className={classes.title}>
          {poll.title}
        </Typography>
        {poll.allowUnregister && (
          <Typography className={classes.pollText}>
            Studentima je omogućeno odustajanje
          </Typography>
        )}

        {poll.allowStudentsAddTopic && (
          <Typography className={classes.pollText}>
            Studentima je omogućeno dodavanje tema
          </Typography>
        )}

        <Typography className={classes.pollText}>
          {poll.applicationStart && (
            <span>Prijave od {dateFormatting(poll.applicationStart)}</span>
          )}
          {poll.applicationEnd && <span> do {dateFormatting(poll.applicationEnd)}</span>}
        </Typography>

        <div className={classes.row}>
          <Typography variant="h5">Teme:</Typography>
          <div className={classes.rightPart}>
            {user.roles.indexOf("ROLE_ADMIN") !== -1 ||
            poll.allowStudentsAddTopic ? (
              <AddTopicModalButton
                pollId={poll.id}
                pollMinStudents={poll.minNumberOfStudents}
                pollMaxStudents={poll.maxNumberOfStudents}
              />
            ) : null}
            {allUsers.length <= 0 ? null : (
              <AssignUserModalButton pollId={poll.id} users={allUsers} />
            )}
            {isAdmin() || isModerator() ? (
              <AddCSVModalButton userId={user.id} pollId={poll.id} />
            ) : null}
          </div>
        </div>

        {isFuture(new Date(poll.applicationStart)) && (
          <div className="alert alert-warning" role="alert">
            Prijave na teme projekta još nisu otvorene
          </div>
        )}

        {isPast(new Date(poll.applicationEnd)) && (
          <div className="alert alert-warning" role="alert">
            Prijave na teme projekta su završene
          </div>
        )}

        {renderTopics()}
      </Container>
    </React.Fragment>
  );
}
