const black = "#000";
const white = "#fff";

export const COLORS = {
  // Basic colors
  black,
  white,

  // Primary colors
  primaryBlue: "#3C98FF",
  primaryGreen: "#28a745",
  primaryRed: "#FC5A5A",
  primaryOrange: "#FF974A",

  // Light colors
  lightRed: "#FFEFEF",
  lightGreen: "#DFF6DA",
  lightBlue: "#E5F2FF",
  lightOrange: "#FFF5ED",

  // Gray shades
  gray_1: "#171725",
  gray_2: "#44444F",
  gray_3: "#696974",
  gray_4: "#92929D",
  gray_5: "#7C7C7C",
  gray_6: "#E2E2EA",
  gray_7: "#eeeeee",
  gray_8: "#F4F7FB"
};
