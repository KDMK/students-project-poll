let prod = {
  API_BASE_URL: "http://35.246.253.234/app",
  LOCAL_AUTH_URL:
    "http://35.246.253.234/app/oauth2/authorize/local?redirect_uri=http://35.246.253.234/app/oauth2/redirect",
  LOGOUT_AUTH_URL: "http://35.246.253.234/auth/logout"
};

let dev = {
  API_BASE_URL: "http://localhost:8080/app",
  LOCAL_AUTH_URL:
    "http://localhost:8080/app/oauth2/authorize/local?redirect_uri=http://localhost:3000/app/oauth2/redirect",
  LOGOUT_AUTH_URL: "http://localhost:8081/auth/logout"
};

export const ACCESS_TOKEN = "accessToken";
export const config = process.env.NODE_ENV === "development" ? dev : prod;
