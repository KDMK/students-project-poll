import { request } from "util/APIUtils";

export const fetchPollsForUser = (userId, langCode) =>
  request({ url: `/app/api/v2/poll/assigned/${userId}?lang=${langCode}` });

export const fetchAllPolls = langCode =>
  request({ url: "/app/api/v2/poll?lang=" + langCode });

export const fetchPollById = id => {
  return request({url: `/app/api/v2/poll/${id}`});
};

export const createPoll = (
  allowStudentsAddTopic,
  allowUnregister,
  applicationEnd,
  applicationStart,
  maxNumberOfStudents,
  minNumberOfStudents,
  multilanguageAttributes,
  topicOrganizationType,
  userId
) => {
  const data = {
    allowStudentsAddTopic,
    allowUnregister,
    applicationEnd,
    applicationStart,
    maxNumberOfStudents,
    minNumberOfStudents,
    multilanguageAttributes,
    topicOrganizationType,
    userId
  };

  return request({
    url: "/app/api/v2/poll",
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json"
    }
  });
};
