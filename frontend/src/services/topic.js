import { request } from "util/APIUtils";

export const fetchTopicsOnPoll = pollId =>
  request({ url: `/app/api/v2/poll/${pollId}/topics` });

export const fetchTopicsByPollId = pollId =>
  request({ url: `/app/api/v2/poll/${pollId}/topics/` });

export const createTopic = (
  title,
  description,
  status,
  type,
  allowUnregister,
  userId,
  minNumberOfStudents,
  maxNumberOfStudents,
  applicationStart,
  applicationEnd,
  pollId
) => {
  const data = {
    title,
    description,
    status,
    type,
    allowUnregister,
    userId,
    minNumberOfStudents,
    maxNumberOfStudents,
    applicationStart,
    applicationEnd,
    pollId
  };

  return request({
    url: "/app/api/v1/topic",
    method: "POST",
    body: JSON.stringify(data)
  });
};

export const assignUserToTopic = (userId, topicId) => {
  return request({
    url: `/app/api/v1/topic/${topicId}/assign/${userId}`,
    method: "PUT"
  });
};

export const approveTopic = (topicId) => {
  return request({
    url: `/app/api/v2/topic/${topicId}/approve`,
    method: "PUT"
  });
};

export const denyTopic = (topicId) => {
  return request({
    url: `/app/api/v2/topic/${topicId}/deny`,
    method: "PUT"
  });
};

export const getTopicsAssignedToUser = userId => {
  return request({
    url: `/app/api/v2/topic/assigned/${userId}`
  });
};

export const isStudentAssignedToTopic = (studentsOnTopic, userId) => {
  for (let student of studentsOnTopic) {
    if (student.id === userId) return true;
  }
  return false;
};

export const unassignUserFromTopic = (userId, topicId) => {
  return request({
    url: `/app/api/v2/topic/${topicId}/remove/${userId}`,
    method: "PUT"
  });
};