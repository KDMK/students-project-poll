import { addDays } from "date-fns";

export const TOPICS = [
  {
    id: 0,
    title: "Istrazivanje o lancu blokova",
    suggestedBy: "Ivan Horvat",
    status: "Odobreno",
    assignedUsers: ["Luka Petrovic", "Nikola Kalinic", "Luka Smek"],
    maxStudentCount: 5,
    type: "grupa",
    deadline: addDays(new Date(), 4)
  },
  {
    id: 1,
    title: "Ekonomija inzinjerstva",
    suggestedBy: "Ivan Horvat",
    status: "Odobreno",
    assignedUsers: ["Luka Petrovic", "Nikola Kalinic", "Luka Smek"],
    maxStudentCount: 6,
    type: "paralelno",
    deadline: addDays(new Date(), 3)
  },
  {
    id: 2,
    title: "Provodenje matricne transformacije",
    suggestedBy: "Ivan Horvat",
    status: "Ceka odobrenje",
    assignedUsers: [],
    maxStudentCount: 4,
    type: "grupa",
    deadline: addDays(new Date(), 5)
  },
  {
    id: 3,
    title: "Dizertacija o programskom jeziku C++",
    suggestedBy: "Ivan Horvat",
    status: "Odbijeno",
    assignedUseres: [],
    maxStudentCount: 3,
    type: "paralelno",
    deadline: addDays(new Date(), 2)
  }
];
