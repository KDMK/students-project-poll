import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import AppRouter from "routers/AppRouter";

function App() {
  return (
    <Router>
      <Route path="/app" component={AppRouter} />
    </Router>
  );
}

export default App;
