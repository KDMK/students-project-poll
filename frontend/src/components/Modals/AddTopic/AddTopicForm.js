import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import {
  Button,
  Input,
  Radio,
  Typography,
  InputLabel,
  Select,
  MenuItem,
  TextField
} from "@material-ui/core";
import DateFnsUtils from "@date-io/date-fns";
import { addDays } from "date-fns";
import { COLORS } from "constants/colors";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider
} from "@material-ui/pickers";
import FormValidator from "../../../validations/FormValidator";
import { createTopic } from "services/topic";
import { getCurrentUser } from "util/APIUtils";
import { ORGANISATION_TYPES } from "constants/types";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  row: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center"
  },
  button: {
    backgroundColor: COLORS.primaryGreen,
    "&:hover": {
      backgroundColor: COLORS.primaryGreen,
      opacity: 0.8
    }
  },
  inputLabel: {
    "&:after": {
      borderBottom: "transparent"
    }
  },
  selectLabel: {
    display: "inline-block",
    marginRight: 8
  },
  select: {
    minWidth: 48,
    "&:after": {
      borderBottom: "transparent"
    }
  },
  error: {
    color: COLORS.primaryRed,
    marginTop: 8
  },
  disabled: {
    color: COLORS.gray_6
  }
}));

export default function AddTopicForm({
  pollId,
  pollMinStudents,
  pollMaxStudents
}) {
  const classes = useStyles();
  const [title, setTitle] = React.useState("");
  const [description, setDescription] = React.useState("");
  const [selectedValue, setSelectedValue] = React.useState(ORGANISATION_TYPES.GROUP);
  const [fromDate, setFromDate] = React.useState(new Date());
  const [toDate, setToDate] = React.useState(addDays(new Date(), 10));
  const [minNumber, setMinNumber] = React.useState(pollMinStudents);
  const [maxNumber, setMaxNumber] = React.useState(pollMaxStudents);
  const [minOpen, setMinOpen] = React.useState(false);
  const [maxOpen, setMaxOpen] = React.useState(false);
  const [validator] = React.useState(new FormValidator());
  const [submitted, setSubmitted] = React.useState(false);

  const handleRadioChange = event => {
    setSelectedValue(event.target.value);
  };

  const handleMinChange = event => {
    setMinNumber(event.target.value);
  };

  const handleMinClose = () => {
    setMinOpen(false);
  };

  const handleMinOpen = () => {
    setMinOpen(true);
  };

  const handleMaxChange = event => {
    setMaxNumber(event.target.value);
  };

  const handleMaxClose = () => {
    setMaxOpen(false);
  };

  const handleMaxOpen = () => {
    setMaxOpen(true);
  };

  const handleSubmit = async event => {
    event.preventDefault();

    setSubmitted(true);

    if (validator.validatePoll(title, minNumber, maxNumber, fromDate, toDate)) {
      const user = await getCurrentUser();
      const status =
        user.roles.indexOf("ROLE_ADMIN") === -1 && user.roles.indexOf("ROLE_MODERATOR") === -1
          ? "WAITING_FOR_APPROVAL"
          : "APPROVED";

      createTopic(
        title,
        description,
        status,
        selectedValue,
        true,
        user.id,
        minNumber,
        maxNumber,
        fromDate,
        toDate,
        pollId
      )
        .then(() => {
          window.location.reload(true);
        })
        .catch(error => console.log(error));
    }
  };

  return (
    <div>
      {submitted &&
        validator.validatePoll(title, minNumber, maxNumber, fromDate, toDate)}
      <div className={classes.root}>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Input
              fullWidth
              placeholder="Naslov"
              onChange={e => setTitle(e.target.value)}
            />
            {submitted && !validator.title.valid && (
              <div className={classes.error}>{validator.title.message}</div>
            )}
          </Grid>
          <Grid item xs={12}>
            <TextField
              id="outlined-multiline-static"
              label="Opis"
              multiline
              rows="4"
              variant="outlined"
              fullWidth
              onChange={e => setDescription(e.target.value)}
            />
          </Grid>
          <Grid item xs={12}>
            <InputLabel id="min-select-label" className={classes.selectLabel}>
              Minimalan broj studenata po temi
            </InputLabel>
            <Select
              labelId="min-select-label"
              id="min-select"
              className={classes.select}
              open={minOpen}
              onClose={handleMinClose}
              onOpen={handleMinOpen}
              value={minNumber}
              onChange={handleMinChange}
            >
              {[...Array(10)].map((x, i) => (
                <MenuItem key={i} value={i + 1}>
                  {i + 1}
                </MenuItem>
              ))}
            </Select>
            {submitted && !validator.minNumber.valid && (
              <div className={classes.error}>{validator.minNumber.message}</div>
            )}
          </Grid>
          <Grid item xs={12}>
            <InputLabel id="max-select-label" className={classes.selectLabel}>
              Maksimalan broj studenata po temi
            </InputLabel>
            <Select
              labelId="max-select-label"
              id="max-select"
              className={classes.select}
              open={maxOpen}
              onClose={handleMaxClose}
              onOpen={handleMaxOpen}
              value={maxNumber}
              onChange={handleMaxChange}
            >
              <MenuItem key="0" value="">
                {" "}
              </MenuItem>
              {[...Array(10)].map((x, i) => (
                <MenuItem key={i + 1} value={i + 1}>
                  {i + 1}
                </MenuItem>
              ))}
            </Select>
            {submitted && !validator.number.valid && (
              <div className={classes.error}>{validator.number.message}</div>
            )}
          </Grid>
          <Grid item xs={12}>
            <div className={classes.row}>
              <Radio
                color={"default"}
                checked={selectedValue === ORGANISATION_TYPES.GROUP}
                onChange={handleRadioChange}
                value={ORGANISATION_TYPES.GROUP}
                inputProps={{ "aria-label": "Grupno" }}
              />
              <Typography style={{ paddingRight: 30 }}>Grupno</Typography>
              <Radio
                color={"default"}
                checked={selectedValue === ORGANISATION_TYPES.PARALLEL}
                onChange={handleRadioChange}
                value={ORGANISATION_TYPES.PARALLEL}
                inputProps={{ "aria-label": "Paralelno" }}
              />
              <Typography>Paralelno</Typography>
            </div>
          </Grid>

          <Grid item xs={12}>
            <Typography>Prijava na temu</Typography>
          </Grid>

          <Grid item xs={6}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <KeyboardDatePicker
                disableToolbar
                variant="inline"
                format="dd/MM/yyyy"
                label="Početak prijave"
                value={fromDate}
                onChange={date => setFromDate(date)}
              />
            </MuiPickersUtilsProvider>
          </Grid>
          <Grid item xs={6}>
            {" "}
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <KeyboardDatePicker
                disableToolbar
                variant="inline"
                format="dd/MM/yyyy"
                label="Završetak prijave"
                value={toDate}
                onChange={date => setToDate(date)}
              />
            </MuiPickersUtilsProvider>
          </Grid>
          {submitted && !validator.date.valid && (
            <div className={classes.error}>{validator.date.message}</div>
          )}
        </Grid>
        <Grid item xs={3} style={{ paddingTop: 20 }}>
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={handleSubmit}
          >
            Spremi
          </Button>
        </Grid>
      </div>
    </div>
  );
}
