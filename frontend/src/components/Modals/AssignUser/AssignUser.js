import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import {COLORS} from "constants/colors";
import Modal from "@material-ui/core/Modal";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import AssignUser from "./AssignUserForm";

const useStyles = makeStyles(theme => ({
  title: {
    paddingBottom: 30
  },
  modal: {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: "50%",
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    borderRadius: 20,
    padding: theme.spacing(2, 4, 3),
    outline: "none",
    overflow: "auto"
  },
  button: {
    height: 40,
    backgroundColor: COLORS.primaryGreen,
    marginLeft: 16,
    "&:hover": {
      backgroundColor: COLORS.primaryGreen,
      opacity: 0.8
    }
  },
  close: {
    position: "absolute",
    right: 32,
    top: 24,
    width: 24,
    height: 24,
    opacity: 0.3,
    "&:hover": {
      opacity: 1,
      cursor: "pointer"
    },
    "&::before": {
      position: "absolute",
      left: 12,
      content: '""',
      height: 24,
      width: 2,
      backgroundColor: COLORS.black,
      transform: "rotate(45deg)"
    },
    "&::after": {
      position: "absolute",
      left: 12,
      content: '""',
      height: 24,
      width: 2,
      backgroundColor: COLORS.black,
      transform: "rotate(-45deg)"
    }
  }
}));

export default function AssignUserModalButton(props) {
  const classes = useStyles();
  // getModalStyle is not a pure function, we roll the style only on the first render
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button
        variant="contained"
        color="primary"
        className={classes.button}
        onClick={handleOpen}
      >
        Dodijeli korisnika projektu
      </Button>
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={open}
        onClose={handleClose}
      >
        <div className={classes.modal}>
          <Typography variant="h4" className={classes.title}>
            Dodijeli korisnika projektu
          </Typography>
          <div className={classes.close} onClick={() => setOpen(false)}></div>
          <AssignUser pollId={props.pollId} users={props} closeModal={() => setOpen(false)}/>
        </div>
      </Modal>
    </div>
  );
}
