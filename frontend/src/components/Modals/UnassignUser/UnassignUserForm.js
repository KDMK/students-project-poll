import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import { Button, } from "@material-ui/core";
import { COLORS } from "constants/colors";
import { unassignUserFromTopic } from "../../../services/topic";
import { getCurrentUser } from "util/APIUtils";

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1
    },
    button: {
        backgroundColor: COLORS.primaryGreen,
        "&:hover": {
            backgroundColor: COLORS.primaryGreen,
            opacity: 0.8
        }
    }
}));

export default function UnassignUser({topicId}) {
    const classes = useStyles();

    const handleSubmit = async event => {
        event.preventDefault();
        const userId = (await getCurrentUser()).id;

        unassignUserFromTopic(userId, topicId)
            .then((response) => {
                console.log(response.id);
                console.log(response.assignedUsers[0])
                window.location.reload();
            })
            .catch((error) => console.log(error))
    };

    return (
        <div className={classes.root}>
            <Grid style={{ textAlign: "center" }}>
                <Button
                    variant="contained"
                    color="primary"
                    className={classes.button}
                    onClick={handleSubmit}
                >
                    Odjavi se
                </Button>
            </Grid>
        </div>

    );
}
