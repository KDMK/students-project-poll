import React, { Component } from "react";
import { Route } from "react-router-dom";

import PollScreen from "screens/PollScreen";
import HomeScreen from "screens/HomeScreen";
import PrivateRoute from "components/PrivateRoute";

import Header from "components/Header";
import OAuth2RedirectHandler from "../services/oauth2/OAuth2RedirectHandler";
import { ACCESS_TOKEN, config } from "../constants";
import { getCurrentUser } from "../util/APIUtils";
import Profile from "../services/profile/Profile";
import CircularProgress from "@material-ui/core/CircularProgress";
import MyTopicScreen from "screens/MyTopicScreen";
import AdminAssign from "screens/AdminAssignScreen";

class AppRouter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      authenticated: false,
      path: props.match.path,
      currentUser: null,
      loading: true
    };

    this.loadCurrentlyLoggedInUser = this.loadCurrentlyLoggedInUser.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
  }

  loadCurrentlyLoggedInUser() {
    this.setState({
      loading: true
    });

    getCurrentUser()
      .then(response => {
        this.setState({
          currentUser: response,
          authenticated: true,
          loading: false
        });
      })
      .catch(error => {
        this.setState({
          error,
          loading: false
        });
      });
  }

  handleLogout() {
    localStorage.removeItem(ACCESS_TOKEN);
    window.location.href = config.LOGOUT_AUTH_URL;
  }

  componentDidMount() {
    this.loadCurrentlyLoggedInUser();
  }

  render() {
    if (this.state.loading) {
      return (
        <div
          style={{
            marginLeft: "auto",
            marginRight: "auto",
            width: "150px",
            paddingTop: "40vh",
            height: "100vh"
          }}
        >
          <CircularProgress size={150} />
        </div>
      );
    }

    return (
      <div style={{ marginTop: "50px" }}>
        <Header
          authenticated={this.state.authenticated}
          onLogout={this.handleLogout}
          user={this.state.currentUser}
        />
        <Route
          path={`${this.state.path}/login`}
          render={() => {
            // Redirect to auth server login
            window.location.href = config.LOCAL_AUTH_URL;
            return null;
          }}
          exact
        />
        <Route
          path={`${this.state.path}/oauth2/redirect`}
          component={OAuth2RedirectHandler}
          exact
        />
        <PrivateRoute
          path={`${this.state.path}/projekt/:projektId`}
          authenticated={this.state.authenticated}
          component={PollScreen}
          user={this.state.currentUser}
          exact
        />
        {this.state.currentUser &&
          this.state.currentUser.roles.indexOf("ROLE_ADMIN") === -1 && this.state.currentUser.roles.indexOf("ROLE_MODERATOR") === -1 && (
            <PrivateRoute
              path={`${this.state.path}/moje_teme`}
              authenticated={this.state.authenticated}
              component={MyTopicScreen}
              user={this.state.currentUser}
              exact
            />
          )}
        <PrivateRoute
          path={`${this.state.path}/profile`}
          authenticated={this.state.authenticated}
          currentUser={this.state.currentUser}
          component={Profile}
        />
        <PrivateRoute
          path={`${this.state.path}/admin/assign`}
          authenticated={this.state.authenticated}
          currentUser={this.state.currentUser}
          component={AdminAssign}
        />
        <PrivateRoute
          path={`${this.state.path}`}
          authenticated={this.state.authenticated}
          component={HomeScreen}
          exact
        />
      </div>
    );
  }
}

export default AppRouter;
